import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardLayoutComponent } from './dashboard-layout.component';
//import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
//import { HttpLoaderFactory } from '../../../app.module';
//import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StoreModule } from '@ngrx/store';
//import { reducers, metaReducers } from '../../store';
import { DashboardLayoutSandbox } from '../../layout/dashboard-layout/dashboard-layout.sandbox';

describe('DashboardLayoutComponent', () => {
  let component: DashboardLayoutComponent;
  let fixture: ComponentFixture<DashboardLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DashboardLayoutComponent,
        //NavigationComponent,
        //HeaderComponent
      ],
      imports: [
        //HttpModule,
        HttpClientModule,
        RouterTestingModule,
        ReactiveFormsModule,
        //NgbModule,
        //StoreModule.forRoot(reducers, { metaReducers }),
      ],
      providers: [
        DashboardLayoutSandbox,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});