import { Component, OnInit } from '@angular/core';
import { DashboardLayoutSandbox } from './dashboard-layout.sandbox';
import { AuthSandbox } from '../../../auth/auth.sandbox';
import { Router } from '@angular/router';
declare const $: any;

@Component({
  selector: 'app-dashboard-layout',
  templateUrl: './dashboard-layout.component.html',
  styleUrls: ['./dashboard-layout.component.scss'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class DashboardLayoutComponent implements OnInit {

  mobileNavPresent = false;
  lastWindowMode: string = $(window).width() > 575 ? "desktop" : "mobile";
  modeHasChanged: boolean = false;
  public navigationMobileVisible: boolean = false;

  constructor(
    public dashboardSandbox: DashboardLayoutSandbox,
    public router: Router) {
  }

  ngOnInit() {
  }

  public toggleNavigation(): void {
    const navigation = $("#desktop-nav");
    const backdrop = $(".backdrop-navigation");

    const show = navigation.css("left") != "0px" ? true : false;
    this.navigationMobileVisible = show;

    navigation.css("left", show ? "0em" : "-3.5em");
    backdrop.css("opacity", show ? "1" : "0");
    backdrop.css("visibility", show ? "visible" : "hidden");
  }
  onResize(event) {
    //En caso de redimensionamiento desktop->mobile->desktop oculte nav.
    const btn = $("#togglerBtn");
    const navigation = $("#desktop-nav");
    const currentWindowMode: string = event.target.innerWidth > 575 ? "desktop" : "mobile";
    const backdrop = $(".backdrop-navigation");

    if (currentWindowMode != this.lastWindowMode) {
      //console.log( "Hubo un cambio en el modo" );
      this.lastWindowMode = currentWindowMode;

      btn.css("left", "0em");
      navigation.css("left", this.lastWindowMode == "mobile" ? "-3.5em" : "0em");
      backdrop.css("opacity", "0");
      backdrop.css("visibility", "hidden");

      if (this.lastWindowMode == "mobile") {
        this.navigationMobileVisible = false;
      }
    }

  }
  clickedInside($event: Event) {
    $event.preventDefault();
    $event.stopPropagation();  // <- that will stop propagation on lower layers
    //console.log("CLICKED INSIDE, MENU WON'T HIDE");
  }

}
