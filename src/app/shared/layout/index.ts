import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardLayoutComponent } from './dashboard-layout/dashboard-layout.component';
import { DashboardLayoutSandbox } from './dashboard-layout/dashboard-layout.sandbox';
import { TranslateModule } from '@ngx-translate/core';


const CONTAINERS = [
  DashboardLayoutComponent
];


@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: CONTAINERS,
  exports: CONTAINERS,
  providers: [DashboardLayoutSandbox]
})
export class LayoutsModule {
}
